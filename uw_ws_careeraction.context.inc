<?php

/**
 * @file
 * uw_ws_careeraction.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ws_careeraction_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cecs_event_block';
  $context->description = 'Displays Happening soon events on the front page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ws_cecs-json-cecs-block-id' => array(
          'module' => 'uw_ws_cecs',
          'delta' => 'json-cecs-block-id',
          'region' => 'content',
          'weight' => '28',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays Happening soon events on the front page');
  $export['cecs_event_block'] = $context;

  return $export;
}
